# README #
Google Analytics Tutorial for Android without configuration file

## What do I need? ##

1. Google tracker Number (such as UA-xxxxxxxx-x)
2. Gradle Project With Google Analytics Lib -OR- use the .jar files provided in the git example if you decide to use intellij instead.

With Gradle setup you can follow example in this link, skip the configuration file.
[Google Analytic for Android](https://developers.google.com/analytics/devguides/collection/android/v4/)

## How do I Setup? ##

### 1. Firstly setup your project with required library ###

### 2. Setup your uses-permission with ACCESS_NETWORK_STATE, INTERNET ###

```
#!java

    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE"></uses-permission>
    <uses-permission android:name="android.permission.INTERNET"></uses-permission>
```

### 3. Setup your Service and Receivers in your manifest.xml -> Application node ###

```
#!xml

        <service
            android:name="com.google.android.gms.analytics.AnalyticsService"
            android:enabled="true"
            android:exported="false"/>
        <!-- Optionally, register AnalyticsReceiver and AnalyticsService to support background
      dispatching on non-Google Play devices -->
        <receiver
            android:name="com.google.android.gms.analytics.AnalyticsReceiver"
            android:enabled="true">
            <intent-filter>
                <action android:name="com.google.android.gms.analytics.ANALYTICS_DISPATCH"/>
            </intent-filter>
        </receiver>

        <!-- Optionally, register CampaignTrackingReceiver and CampaignTrackingService to enable
             installation campaign reporting -->
        <receiver
            android:name="com.google.android.gms.analytics.CampaignTrackingReceiver"
            android:exported="true">
            <intent-filter>
                <action android:name="com.android.vending.INSTALL_REFERRER"/>
            </intent-filter>
        </receiver>
        <service android:name="com.google.android.gms.analytics.CampaignTrackingService"/>
```


### 4. Setup your Application class  ###


In your Application class you will need to define your tracker ID and initialize it.
Without this class, google analytic will not be able to register your session. **This is mandatory.**

Note that you will need to make your tracker accessible somewhere, or use it as a singleton if you need to.

```
#!java

public class AnalyticsApplication extends Application {

    private Tracker mTracker;

    /**
     * Gets the default {@link Tracker} for this {@link Application}.
     * @return tracker
     */
    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);

            //set your dispatch time for your google analytic, this will be the realtime responsiveness value
            //Here we set to 5 seconds to reduce number of out going requests.
            analytics.setLocalDispatchPeriod(5);

            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            //create a tracker with your tracker id number
            mTracker = analytics.newTracker("UA-xxxxxxxx-x");
        }
        return mTracker;
    }

}
```


Then link to your mantifest.xml




```
#!xml

<application

        android:label="@string/app_name"
        android:icon="@drawable/ic_launcher"
        android:name=".AnalyticsApplication">       <-----------here

...
```



### 5. Adding events ###

This will appear on your Event section of google analytic

```
#!java

mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory("Action")
                        .setAction("Eat Icecream")
                        .setLabel(b.getText() + "")
                        .build());
```
### 6. Adding Screen name ###

This will appear on your Screen section of google analytic

```
#!java

mTracker.setScreenName("my photo viewing " + file.name);
mTracker.send(new HitBuilders.ScreenViewBuilder().build());

```