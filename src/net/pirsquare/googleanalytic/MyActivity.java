package net.pirsquare.googleanalytic;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.Date;

public class MyActivity extends Activity {

    private Tracker mTracker;


    synchronized public Tracker getDefaultTracker() {

        if (mTracker == null) {
            // Obtain the shared Tracker instance.
            AnalyticsApplication application = (AnalyticsApplication) getApplication();
            mTracker = application.getDefaultTracker();
        }

        return mTracker;
    }
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        getDefaultTracker();

        Button red = (Button)findViewById(R.id.red);
        Button purple = (Button)findViewById(R.id.purple);
        Button blue = (Button)findViewById(R.id.blue);
        Button green = (Button)findViewById(R.id.green);


        //event
        View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Button b = (Button)v;

                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory("Action")
                        .setAction(new Date().toString())
                        .setLabel(b.getText() + "")
                        .build());
            }
        };

        red.setOnClickListener(clickListener);
        purple.setOnClickListener(clickListener);
        blue.setOnClickListener(clickListener );
        green.setOnClickListener(clickListener );


    }
}
